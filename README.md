## COMMENT EXECUTER LE CODE

- Clonez le repository avec __git clone__
- Import ou executer le fichier __articles_db.sql__ dans __phpMyAdmin / MYSQL Workbench__ ou n'importe quel client MySQL
  - Ceci permettra de creer la base de donnees ainsi que les tables
- Faites un cd dans le dossier __articles__ et executez les etapes suivantes :
  - Entrez dans le fichier __connections.js__ pour mettre les credentials d'acces a votre base de donnees.
    - __host: 'localhost',__
    - __user: '', // USERNAME__
    - __password: '', // PASSWWORD__
    - __database: '' // DB NAME__
  - Executer la commande __npm install__ pour install le dependencies --> Une fois termine:
  - Executer la commande __npm run dev__ pour lancer le server


### 1. LANCEMENT DE L'APPLICATION
![alt text](testing_screenshots/cmd_dev.PNG)

### 2 . ENDPOINT --> /api/import
![alt text](testing_screenshots/api_1.PNG)

### 2 . ENDPOINT --> /api/articles
![alt text](testing_screenshots/api_2.PNG)
- Resultats dans la table importations
![alt text](testing_screenshots/select_1.PNG)
- Resultats dans la table articles
![alt text](testing_screenshots/select_2.PNG)
![alt text](testing_screenshots/select_3.PNG)

// IMPORT LIBRARIES
const express = require('express');
const utils = require("./mainUtils");
const connect = require("./dbUtils");
var dbconn = require('./connection');

var dbconn = require('./connection');

// NODE SERVER CONFIG STARTS
const app = express();
const PORT = 30001;

app.listen(PORT, function() {
  console.log('listening on ' + PORT);
})

// NODE SERVER CONFIG ENDS


// WEB ROUTING STARTS

/**
 * Create New articles
 * METHOD : post
 * @return response()
 */
app.post('/api/import', (req, res) => {

     try {
       utils.runXMLFeedImporter();
       msg = "Articles inserted successfully !";
       res.send(api201CreatedResponse(msg));
     } catch (e) {
       console.log(e)
       msg = "UNKNOWN_ERROR_OCCURED";
       res.send(api500ServerErrorResponse(msg));
     }


})


/**
 * Get All articles
 * METHOD : get
 * @return response()
 */
app.get('/api/articles', (req, res) => {
  let sqlQuery = "SELECT * FROM articles_table ORDER BY importDate DESC;";
  let query = dbconn.query(sqlQuery, (err, results) => {
    if (err) {
           console.log("internal error", err);
           return;
       }
    var rows = JSON.parse(JSON.stringify(results));
    res.send(api200KResponse(rows))

  });

})

// WEB ROUTING ENDS


/**
 * API Responses
 *
 * @return response()
 */

function api201CreatedResponse(msg){
    return JSON.stringify({"message": msg, "status": 201});
}

function api200KResponse(fetched_data){
    return JSON.stringify({"data": fetched_data, "status": 200});
}

function api500ServerErrorResponse(msg){
    return JSON.stringify({"message": msg, "status": 500});
}

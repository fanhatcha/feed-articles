const dbUtils = require("./dbUtils");

let Parser = require('rss-parser');
let parser = new Parser();

function runXMLFeedImporter(){

  (async () => {
    let rawXML = await parser.parseURL('https://www.lemonde.fr/rss/une.xml');

    // Store raw XML feed + Articles in DB
    insertRawXML = dbUtils.storeRawXMLInDB(rawXML)
    rawXML.items.forEach((article) => {
          insertArticle = dbUtils.storeArticlesInDB(article);
    });

  })();

}





module.exports = {
  runXMLFeedImporter
};

let mysql = require('mysql');


let pool = mysql.createPool({
    connectionLimit: 5,
    host: 'localhost',
    user: '', // USERNAME
    password: '', // PASSWWORD
    database: 'articles_db'
});


pool.getConnection((err,connection)=> {
  if(err)
  throw err;
  console.log('Database connected successfully');
  connection.release();
});

module.exports = pool;

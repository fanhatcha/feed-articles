// IMPORT LIBRARIES
var dbconn = require('./connection');

let now = new Date();


function storeRawXMLInDB(rawArticles){

  let insertableData = {date_importation:now, rawContent:JSON.stringify(rawArticles)};
  let sqlQuery = "INSERT INTO importations_table SET ?";

  let query = dbconn.query(sqlQuery, insertableData,(err, results) => {
    if(err) throw err;
  });



}

function storeArticlesInDB(article){
  let sqlQuery = `SELECT COUNT(1) AS articlesCount FROM articles_table WHERE externalId='${article.guid}'`;

  let query = dbconn.query(sqlQuery, (err, results) => {
   if(err) throw err;

   numArticles = results[0].articlesCount;

   // if article not presents in db
   if(numArticles == 0){
     let sqlInsertQuery = "INSERT INTO articles_table SET ?";
     insertableData = {
                       externalId:article.guid, importDate:now, titre:article.title,
                       descriptif:article.contentSnippet, date_de_publication:article.isoDate,
                       lien:article.link, MainPicture:article.content
                     }
     let insertQuery = dbconn.query(sqlInsertQuery, insertableData,(error, results) => {
       if(error) throw error;
     });
   }

 });

}



module.exports = {
  storeRawXMLInDB,
  storeArticlesInDB
};
